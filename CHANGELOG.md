# Changelog

All notable changes to this project will be documented in this file. See [standard-version](https://github.com/conventional-changelog/standard-version) for commit guidelines.

### [0.1.5](https://gitlab.com/beautils/ipc/compare/v0.1.4...v0.1.5) (2021-02-27)


### Features

* **core:** use single running instance instead of single instance instantiation ([73ba95b](https://gitlab.com/beautils/ipc/commit/73ba95b56959ea7cd4653c3edc112baeb36bd0de))


### Bug Fixes

* **core:** dont lock instance in constructor and return boolean on unsubscribe ([e6d7af8](https://gitlab.com/beautils/ipc/commit/e6d7af872e0272a4d3cd4d5b55991092fe6132d2))

### [0.1.4](https://gitlab.com/beautils/ipc/compare/v0.1.3...v0.1.4) (2021-02-21)

### [0.1.3](https://gitlab.com/beautils/ipc/compare/v0.1.2...v0.1.3) (2021-02-21)


### Features

* new APIs for server and client ([eb0fe84](https://gitlab.com/beautils/ipc/commit/eb0fe84b5b91fe03094fa73d1d882f0d90286e69))
