import "source-map-support/register";
import { Client } from "../lib/Client";
import { BAR_EVENT, CLIENT_ID, DOUBLE_VALUE, FOO_EVENT, HALVE_VALUE, SERVER_ID } from "./constants";
import { Config } from "../lib/common";

(async () => {
    const config: Config = { debug: true };
    const suffix: string = process.argv[2];
    const client: Client = await Client.start(SERVER_ID, CLIENT_ID + "-" + suffix, config);

    // // EX 1
    // client.onRequest(DOUBLE_VALUE, async (sender: string, num: number) => {
    //     console.log('got req', sender, num);
    //     return num / 2;
    // });

    // // EX 2
    // client.onRequest(HALVE_VALUE, async (sender: string, num: number) => num / 2);
    // const num: number = Math.random() ;
    // const doubled: number = await client.request(DOUBLE_VALUE, num);
    // console.log('num', num, 'doubled', doubled);

    // // EX 3
    // client.subscribe(BAR_EVENT, (senderId: string, data: number) => {
    //     console.log(`BAR_EVENT: ${client.id} received ${data} from ${senderId}`);
    // });
    // setInterval( () => {
    //     client.send(FOO_EVENT, Math.random().toFixed(2));
    // }, 1000);
})();
