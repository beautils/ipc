export const SERVER_ID: string = "server";
export const CLIENT_ID: string = "client";

export const FOO_EVENT: string = "foo";
export const BAR_EVENT: string = "bar";

export const DOUBLE_VALUE: string = "double";
export const HALVE_VALUE: string = "halve";
