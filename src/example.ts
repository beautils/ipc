import "source-map-support/register";
import * as child_process from "child_process";
import {Config} from './lib/common';
import {Server} from './lib/Server';
import {DOUBLE_VALUE, HALVE_VALUE, SERVER_ID} from './example/constants';

(async () => {
    console.log("example start");

    const config: Config = {debug: true};
    const server: Server = await Server.start(SERVER_ID, config);

    // // EX 1
    // setTimeout(async () => {
    //     const doubled: number = await server.request(DOUBLE_VALUE, 5);
    // }, 1000);

    // // EX 2
    // server.onRequest(DOUBLE_VALUE, async (sender: string, num: number) => {
    //     const halved: number = await server.request(HALVE_VALUE, num);
    //     return halved * 2;
    // });

    // // EX 3
    // const subId: string = server.subscribe(FOO_EVENT, (senderId: string, data: number) => {
    //     console.log(`FOO_EVENT: ${server.id} received ${data} from ${senderId}`);
    // });
    // setTimeout(() => {
    //     server.unsubscribe(subId);
    // }, 10000);
    // setInterval(() => {
    //     server.send(BAR_EVENT, Math.random().toFixed(2));
    // }, 5000);

    const numClients: number = 3;
    for (let i = 1; i <= numClients; i++) {
        setTimeout(
            () => {
                child_process.fork("./dist/example/run-client.js", ["nr-" + i]);
            },
            // (i - 1) * 2000
            0
        );
    }
})();
