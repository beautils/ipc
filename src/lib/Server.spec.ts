import {Server} from './index';
import 'jest-extended';

describe("Server", () => {

    const serverId: string = 'the-server';

    describe("startup and shutdown", () => {

        it("starts", async () => {
            // arrange
            const server: Server = new Server(serverId);
            // act
            const start: Promise<Server> = server.start();
            // assert
            await expect(start).toResolve();
            // cleanup
            await server.stop();
        });

        it("stops", async () => {
            // arrange
            const server: Server = new Server(serverId);
            await server.start();
            // act
            const stop: Promise<Server> = server.stop();
            // assert
            await expect(stop).toResolve();
        });

        it("doesn't allow to start mutiple instances", async () => {
            // arrange
            const server: Server = new Server(serverId);
            await server.start();
            const server2: Server = new Server('another-id');
            // act
            const start2: Promise<Server> = server2.start();
            // assert
            await expect(start2).toReject();
            // cleanup
            await server.stop();
        });

    });

    describe("when is running", () => {

        let server: Server;

        beforeEach(async () => {
            server = new Server(serverId);
            await server.start();
        });

        afterEach(async () => {
            await server.stop();
        });

        it("publishs", async () => {
            // arrange
            const event: string = 'foo';
            const data: any = {foo: 'bar'};
            // act
            const result: void = server.publish(event, data);
            // assert
            expect(result).toBeUndefined();
        });

        it("subscribes", async () => {
            // arrange
            const event: string = 'bar';
            // act
            const result: string = server.subscribe(event, () => {});
            // assert
            expect(result).toBeString();
        });

        it("subscribes once", async () => {
            // arrange
            const event: string = 'bar';
            // act
            const result: string = server.subscribeOnce(event, () => {});
            // assert
            expect(result).toBeString();
        });

        it("unsubscribes", async () => {
            // arrange
            const event: string = 'foo';
            const subId: string = server.subscribe(event, () => {});
            const nonExistingSubId: string = 'non-existing-subscription-id';
            // act
            const result: boolean = server.unsubscribe(subId);
            const nonExistingSubIdResult: boolean = server.unsubscribe(nonExistingSubId);
            // assert
            expect(result).toBeTrue();
            expect(nonExistingSubIdResult).toBeFalse();
        });

    });

});


