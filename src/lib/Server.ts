import { Core } from "./Core";
import { Config, MessageDto, EventName, NodeIpcServer, NodeIpcStatic } from "./common";
import { Socket } from "net";

export class Server extends Core<NodeIpcServer> {
    public static async start(id: string, config?: Config): Promise<Server> {
        return new Server(id, config).start();
    }

    public constructor(id: string, config?: Config) {
        super(id, config);
    }

    protected initialize(): void {
        // this.logger.log('Server.initialize', this.id);
        NodeIpcStatic.serve();
        this.initializeNodeIpcInstance(<NodeIpcServer>NodeIpcStatic.server);
        this.nodeIpcInstance.start();
    }

    protected terminate(): void {
        this.logger.log("Server.terminate", this.id);
        this.nodeIpcInstance.stop();
        this.finalizeNodeIpcInstance();
        this.resolve();
    }

    protected registerEventListeners(): void {
        this.nodeIpcInstance.on(EventName.START, this.onStart.bind(this));
        super.registerEventListeners();
    }

    protected unregisterEventListeners(): void {
        this.nodeIpcInstance.off(EventName.START, this.onStart.bind(this));
        super.unregisterEventListeners();
    }

    protected onStart(): void {
        this.logger.log("Server.onStart", this.id);
        this.resolve();
    }

    protected send<T>(event: string, dto: MessageDto<T>): void {
        this.nodeIpcInstance.sockets.forEach((socket: Socket) => {
            this.nodeIpcInstance.emit(socket, event, dto);
        });
    }
}
