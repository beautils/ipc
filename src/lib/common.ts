import { Socket } from "net";
import nodeIpc from "node-ipc";

export type ResolveFn<T> = (value: T) => void;
export type RejectFn = (reason?: any) => void;

type TypeOfNodeIpc = typeof nodeIpc;
export const NodeIpcStatic: TypeOfNodeIpc = nodeIpc;

export interface Config {
    debug?: boolean;
}

export const DefaultConfig: Config = {
    debug: false,
};

export enum EventName {
    ERROR = "error",
    CONNECT = "connect",
    DISCONNECT = "disconnect",
    DESTROY = "destroy",
    SOCKET_DISCONNECTED = "socket.disconnected",
    DATA = "data",
    START = "start",
}

export const USER_EVENT_PREFIX: string = "_";
export const REQUEST_EVENT_PREFIX: string = "req_";
export const RESPONSE_EVENT_PREFIX: string = "res_";

export const ERROR_INSTANCE_LOCKED: string = "Only one instance of IPC per process allowed";
export const ERROR_SAME_ID: string = "Client IPC must have another id than server";
export const ERROR_STATE_IS_NOT_STARTED: string = "IPC is not started";
export const ERROR_STATE_IS_NOT_STOPPED: string = "IPC is not stopped";
export const ERROR_REQUEST_TIMEOUT: string = "request timed out";

export interface MessageDto<T> {
    event: string;
    senderId: string;
    data: T;
}

type SocketOr<Npi extends NodeIpcInstance, Other> = Npi extends NodeIpcServer ? Socket : Other;
type SocketOrNever<Npi extends NodeIpcInstance> = SocketOr<Npi, never>;
type ErrorCb = (err: any) => void;
type SocketDisconnectedCb = (socket: Socket, destroyedSocketID: string) => void;
type DataCb = (data: Buffer) => void;
type EmptyCb = () => void;
type SocketOrEmptyCb<Npi extends NodeIpcInstance> = (socket: SocketOrNever<Npi>) => void;
export type CustomEventCb<T> = (dto: MessageDto<T>, socket?: Socket) => void;

export type NodeIpcServer = typeof NodeIpcStatic.server & {
    on(event: EventName.START, cb: EmptyCb): void;
    on(event: string, cb: CustomEventCb<any>): void;
    off(event: EventName.START, cb: EmptyCb): void;
    off(event: string, cb: CustomEventCb<any>): void;
    sockets: Socket[];
};
export type NodeIpcClient = {
    on(event: EventName.ERROR, cb: ErrorCb): void;
    on(event: EventName.CONNECT, cb: SocketOrEmptyCb<NodeIpcClient>): void;
    on(event: EventName.DISCONNECT, cb: SocketOrEmptyCb<NodeIpcClient>): void;
    on(event: EventName.DESTROY, cb: SocketOrEmptyCb<NodeIpcClient>): void;
    on(event: EventName.SOCKET_DISCONNECTED, cb: SocketDisconnectedCb): void;
    on(event: EventName.DATA, cb: DataCb): void;
    on(event: string, cb: CustomEventCb<any>): void;
    off(event: EventName.ERROR, cb: ErrorCb): NodeIpcClient;
    off(event: EventName.CONNECT, cb: SocketOrEmptyCb<NodeIpcClient>): void;
    off(event: EventName.DISCONNECT, cb: SocketOrEmptyCb<NodeIpcClient>): void;
    off(event: EventName.DESTROY, cb: SocketOrEmptyCb<NodeIpcClient>): void;
    off(event: EventName.SOCKET_DISCONNECTED, cb: SocketDisconnectedCb): void;
    off(event: EventName.DATA, cb: DataCb): void;
    off(event: string, cb: CustomEventCb<any>): void;
    emit(event: string, value?: any): void;
};

export type NodeIpcInstance = NodeIpcClient | NodeIpcServer;

export enum State {
    STOPPED = "STOPPED",
    STARTED = "STARTED",
    STARTING = "STARTING",
    STOPPING = "STOPPING",
}

export interface Subscription<T> {
    id: string;
    event: string;
    cb: CustomEventCb<T>;
}

export type SubcribeCb<T> = (senderId: string, data: T) => void;

export interface RequestDto<T> {
    id: string;
    data: T;
}

export interface ResponseDto<T> {
    id: string;
    data: T;
}

export type CreateResponseFn<Treq, Tres> = (sender: string, data: Treq) => Tres | Promise<Tres>;
