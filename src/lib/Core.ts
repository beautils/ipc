import {
    Config,
    CreateResponseFn,
    CustomEventCb,
    DefaultConfig,
    ERROR_INSTANCE_LOCKED,
    ERROR_REQUEST_TIMEOUT,
    ERROR_STATE_IS_NOT_STARTED,
    ERROR_STATE_IS_NOT_STOPPED,
    EventName,
    MessageDto,
    NodeIpcInstance,
    NodeIpcStatic,
    RejectFn,
    REQUEST_EVENT_PREFIX,
    RequestDto,
    ResolveFn,
    RESPONSE_EVENT_PREFIX,
    ResponseDto,
    State,
    SubcribeCb,
    Subscription,
    USER_EVENT_PREFIX,
} from "./common";
import assert from "assert";
import {Socket} from "net";
import {Logger} from "./Logger";
import {v1 as uuidV1} from "uuid";

export abstract class Core<Npi extends NodeIpcInstance> {

    private static RUNNING_INSTANCE: Core<NodeIpcInstance>;

    protected readonly logger: Logger;

    private _resolve: ResolveFn<void>;
    private _reject: RejectFn;

    private _nodeIpcInstance: Npi;
    private _state: State = State.STOPPED;
    private readonly _config: Config;

    private readonly subscriptions: Map<string, Subscription<any>> = new Map<string, Subscription<any>>();

    protected get config(): Config {
        return this._config;
    }

    protected get nodeIpcInstance(): Npi {
        return this._nodeIpcInstance;
    }

    protected get state(): State {
        return this._state;
    }

    public get id(): string {
        return NodeIpcStatic.config.id;
    }

    protected abstract initialize(): void;

    protected abstract terminate(): void;

    protected abstract send<T>(event: string, dto: MessageDto<T>): void;

    protected constructor(id: string, config?: Config) {
        this._config = {
            ...DefaultConfig,
            ...config,
        };
        this.logger = new Logger(this._config.debug);
        NodeIpcStatic.config = {
            ...NodeIpcStatic.config,
            id: id,
            maxRetries: 0,
            silent: true,
        };
    }

    public async start(): Promise<this> {
        this.assertNoRunningInstance();
        this.assertStateStopped();
        Core.RUNNING_INSTANCE = this;
        this.setState(State.STARTING);
        await this.run(this.initialize.bind(this));
        this.setState(State.STARTED);
        return this;
    }

    public async stop(): Promise<this> {
        this.assertStateStarted();
        this.setState(State.STOPPING);
        await this.run(this.terminate.bind(this));
        Core.RUNNING_INSTANCE = undefined;
        this.setState(State.STOPPED);
        return this;
    }

    public subscribeOnce<T>(event: string, subscribeCb: SubcribeCb<T>): string {
        this.assertStateStarted();
        const subId: string = this.subscribe(event, (senderId: string, data: T) => {
            this.unsubscribe(subId);
            subscribeCb(senderId, data);
        });
        return subId;
    }

    public subscribe<T>(event: string, subscribeCb: SubcribeCb<T>): string {
        this.assertStateStarted();
        const subscriptionId: string = uuidV1();
        const eventWithPrefix: string = USER_EVENT_PREFIX + event;
        const customEventCb: CustomEventCb<T> = (messageDto: MessageDto<T>, socket?: Socket) => {
            subscribeCb(messageDto.senderId, messageDto.data);
        };
        const subscription: Subscription<T> = {
            id: subscriptionId,
            event: eventWithPrefix,
            cb: customEventCb,
        };
        this.subscriptions.set(subscriptionId, subscription);
        this.nodeIpcInstance.on(eventWithPrefix, customEventCb);
        return subscriptionId;
    }

    public unsubscribe<T>(subscriptionId: string): boolean {
        this.assertStateStarted();
        const subscription: Subscription<T> = this.subscriptions.get(subscriptionId);
        if (!subscription) return false;
        this.nodeIpcInstance.off(subscription.event, subscription.cb);
        return this.subscriptions.delete(subscriptionId);
    }

    public publish<T>(event: string, data?: T): void {
        this.assertStateStarted();
        const eventWithPrefix: string = USER_EVENT_PREFIX + event;
        const messageDto: MessageDto<T> = {
            event: eventWithPrefix,
            senderId: this.id,
            data: data,
        };
        this.send(eventWithPrefix, messageDto);
    }

    public request<Treq, Tres>(event: string, data?: Treq, timeoutMs: number = Infinity): Promise<Tres> {
        this.assertStateStarted();
        return new Promise<Tres>((resolve: ResolveFn<Tres>, reject: RejectFn) => {
            let timeout: NodeJS.Timeout;
            const requestEvent: string = REQUEST_EVENT_PREFIX + event;
            const responseEvent: string = RESPONSE_EVENT_PREFIX + event;
            const requestId: string = uuidV1() + process.pid;
            const subscriptionId: string = this.subscribe(
                responseEvent,
                (senderId: string, responseDto: ResponseDto<Tres>) => {
                    if (responseDto.id === requestId) {
                        this.unsubscribe(subscriptionId);
                        clearTimeout(timeout);
                        resolve(responseDto.data);
                    }
                }
            );
            const requestDto: RequestDto<Treq> = {
                id: requestId,
                data: data,
            };
            this.publish(requestEvent, requestDto);
            if (timeoutMs === Infinity) return;
            timeout = setTimeout(() => {
                this.unsubscribe(subscriptionId);
                reject(new Error(ERROR_REQUEST_TIMEOUT));
            }, timeoutMs);
        });
    }

    public respond<Treq, Tres>(event: string, createResponse: CreateResponseFn<Treq, Tres>): void {
        this.assertStateStarted();
        const requestEvent: string = REQUEST_EVENT_PREFIX + event;
        const responseEvent: string = RESPONSE_EVENT_PREFIX + event;
        this.subscribe(requestEvent, async (senderId: string, requestDto: RequestDto<Treq>) => {
            const createResponseResult: Tres | Promise<Tres> = createResponse(senderId, requestDto.data);
            const responseData: Tres =
                createResponseResult instanceof Promise ? await createResponseResult : createResponseResult;
            const responseDto: ResponseDto<Tres> = {
                id: requestDto.id,
                data: responseData,
            };
            this.publish(responseEvent, responseDto);
        });
    }

    protected resolve(): void {
        if (!this._resolve) return this.onUnknownError();
        this._reject = undefined;
        const resolve: ResolveFn<void> = this._resolve;
        this._resolve = undefined;
        resolve();
    }

    protected reject(err: Error): void {
        if (!this._reject) return this.onUnknownError(err);
        this._resolve = undefined;
        const reject: RejectFn = this._reject;
        this._reject = undefined;
        reject(err);
    }

    protected assertNoRunningInstance(): void {
        assert(!Core.RUNNING_INSTANCE, ERROR_INSTANCE_LOCKED);
    }

    protected assertStateStarted(): void {
        assert(this.state === State.STARTED, `${ERROR_STATE_IS_NOT_STARTED}: ${this.id}`);
    }

    protected assertStateStopped(): void {
        assert(this.state === State.STOPPED, `${ERROR_STATE_IS_NOT_STOPPED}: ${this.id}`);
    }

    protected setState(value: State): void {
        this._state = value;
    }

    protected initializeNodeIpcInstance(nodeIpcInstance: Npi) {
        if (!!this.nodeIpcInstance) this.unregisterEventListeners();
        this._nodeIpcInstance = nodeIpcInstance;
        this.registerEventListeners();
    }

    protected finalizeNodeIpcInstance() {
        assert(!!this.nodeIpcInstance);
        this.unregisterEventListeners();
        this._nodeIpcInstance = undefined;
    }

    protected registerEventListeners(): void {
        this.nodeIpcInstance.on(EventName.ERROR, this.onError.bind(this));
        this.nodeIpcInstance.on(EventName.CONNECT, this.onConnect.bind(this));
        this.nodeIpcInstance.on(EventName.DISCONNECT, this.onDisconnect.bind(this));
        this.nodeIpcInstance.on(EventName.DESTROY, this.onDestroy.bind(this));
        this.nodeIpcInstance.on(EventName.SOCKET_DISCONNECTED, this.onSocketDisconnected.bind(this));
        this.nodeIpcInstance.on(EventName.DATA, this.onData.bind(this));
    }

    protected unregisterEventListeners(): void {
        this.nodeIpcInstance.off(EventName.ERROR, this.onError.bind(this));
        this.nodeIpcInstance.off(EventName.CONNECT, this.onConnect.bind(this));
        this.nodeIpcInstance.off(EventName.DISCONNECT, this.onDisconnect.bind(this));
        this.nodeIpcInstance.off(EventName.DESTROY, this.onDestroy.bind(this));
        this.nodeIpcInstance.off(EventName.SOCKET_DISCONNECTED, this.onSocketDisconnected.bind(this));
        this.nodeIpcInstance.off(EventName.DATA, this.onData.bind(this));
    }

    protected onError(error: any): void {
        if (this.state === State.STARTING || this.state === State.STOPPING) return this.reject(error);
        this.onUnknownError(error);
    }

    protected onConnect(socket?: Socket): void {
        this.debug_OnEvent(EventName.CONNECT, ...arguments);
    }

    protected onDisconnect(socket?: Socket): void {
        this.debug_OnEvent(EventName.DISCONNECT, ...arguments);
    }

    protected onDestroy(socket?: Socket): void {
        this.debug_OnEvent(EventName.DESTROY, ...arguments);
    }

    protected onSocketDisconnected(socket: Socket): void {
        this.debug_OnEvent(EventName.SOCKET_DISCONNECTED, ...arguments);
    }

    protected onData(data: Buffer): void {
        this.debug_OnEvent(EventName.DATA, ...arguments);
    }

    private debug_OnEvent(event: string, ...data: any[]) {
        // this.logger.log('Core.onEvent', event, this.id, ...data);
        this.logger.log("Core.debugOnEvent", event, this.id);
    }

    private onUnknownError(error?: any) {
        if (error.code === "EPIPE" || error.message?.startsWith("unknown_clientid")) {
            this.logger.error("Core.onUnknownError", this.id, error);
            return;
        }
        throw new Error(error?.toString());
    }

    private run(runFn: () => void): Promise<void> {
        return new Promise<void>((resolve: ResolveFn<void>, reject: RejectFn) => {
            this._resolve = resolve;
            this._reject = reject;
            return runFn();
        });
    }
}
