module.exports = {
    "verbose": true,
    "roots": [
        "<rootDir>/src"
    ],
    "testMatch": [
        "<rootDir>/**/__tests__/**/*.+(ts|tsx|js)",
        "<rootDir>/**/?(*.)+(spec|test).+(ts|tsx|js)"
    ],
    "transform": {
        "^.+\\.(ts|tsx)$": "ts-jest"
    }
}
